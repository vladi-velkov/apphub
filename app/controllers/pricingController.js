( function () {

  var pricingController = function ($scope, $routeParams, $location, pricingFactory)
      {
        var packageId = $routeParams.packageId;
        var myLocation = $location.path();
        console.log(myLocation);
        $scope.packageItems = [];
        $scope.menuItems = [];
        $scope.user = {};
        $scope.user.id = pricingFactory.getUsersLength();

        $scope.update = function(user) 
          {
            $scope.user = angular.copy(user);
            console.log($scope.user);
            pricingFactory.setUser($scope.user);
            $location.path(myLocation);
          };

        function init(){
            $scope.packageItems = pricingFactory.getPackage(packageId);
            $scope.packagesItems = pricingFactory.getPackages();
            $scope.menuItems = pricingFactory.getMenuItems();
        };

        init();



      };

  pricingController.$inject = ['$scope', '$routeParams', '$location', 'pricingFactory'];

  angular.module('bizHubApp')
    .controller('pricingController', pricingController);
      
}());