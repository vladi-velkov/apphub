( function () {

  var createCompanyController = function ($scope, chooseCompanyFactory)
      {
        $scope.verticalNavigationItems = [
          {name:'Dashboard', class:'fa fa-home', link:'#'}
        ];

        $scope.master = {};
        $scope.test = [];
        $scope.master.id = chooseCompanyFactory.getCompaniesLength();

        $scope.update = function(company) 
          {
            $scope.master = angular.copy(company);
            console.log($scope.master);
            chooseCompanyFactory.setCompanies($scope.master);
          };

        $scope.reset = function(form) 
          {
            if (form) 
              {
                form.$setPristine();
                form.$setUntouched();
              }
            $scope.company = angular.copy($scope.master);
          };

        $scope.reset();




      };

  createCompanyController.$inject = ['$scope', 'chooseCompanyFactory'];

  angular.module('bizHubApp')
    .controller('createCompanyController', createCompanyController);
      
}());



