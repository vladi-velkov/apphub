( function () {

  var dashboardController = function ($scope, $routeParams, modulesFactory)
      {
        $scope.verticalNavigationItems = [
          {name:'Dashboard', class:'fa fa-home', link:'#'}
        ];

        var moduleId = $routeParams.moduleId;
        var appId = $routeParams.appId;
        var companyId = $routeParams.companyId;

        $scope.myAppModules = [];
        $scope.myAppModule = [];
        $scope.myAppApp = [];
        $scope.myAppCompany = []; 


        function init(){
            $scope.myAppModule = modulesFactory.getModule(companyId, appId, moduleId);
            $scope.myAppModules = modulesFactory.getModules(companyId, appId);
            $scope.myAppApp = modulesFactory.getApp(companyId, appId);
            $scope.myAppApps = modulesFactory.getApps(companyId);
            $scope.myAppCompany = modulesFactory.getCompany(companyId);
            $scope.myAppCompanies = modulesFactory.getCompanies();
        };

        init();

        $scope.userList = [
          {id: 1, name:'Владислав Велков', picture:'img/bg-close-person-hi.png', position:'Управител', phone:'0877 125 425', mail:'vladislav@rightclick.bg', type:'Администратор', accessTo:[{app:'Склад 1'},{app:'Склад 4'},{app:'Склад 3'}]},
          {id: 2, name:'Владислав Велков', picture:'img/bg-close-person-hi.png', position:'Управител', phone:'0877 125 425', mail:'vladislav@rightclick.bg', type:'Администратор', accessTo:'Склад 1'},
          {id: 3, name:'Владислав Велков', picture:'img/bg-close-person-hi.png', position:'Управител', phone:'0877 125 425', mail:'vladislav@rightclick.bg', type:'Администратор', accessTo:'Склад 1'},
          {id: 4, name:'Владислав Велков', picture:'img/bg-close-person-hi.png', position:'Управител', phone:'0877 125 425', mail:'vladislav@rightclick.bg', type:'Администратор', accessTo:'Склад 1'},
          {id: 5, name:'Владислав Велков', picture:'img/bg-close-person-hi.png', position:'Управител', phone:'0877 125 425', mail:'vladislav@rightclick.bg', type:'Администратор', accessTo:'Склад 1'}
        ];

      };

  dashboardController.$inject = ['$scope', '$routeParams', 'modulesFactory'];

  angular.module('bizHubApp')
    .controller('dashboardController', dashboardController);
      
}());



