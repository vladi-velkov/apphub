( function () {

  var landingController = function ($scope)
      {
        $scope.menuItems = [
          {name:'Начало', class:'active', link:'#/'},
          {name:'Функционалности', class:'', link:'#'},
          {name:'Цени', class:'', link:'#/pricing'},
          {name:'Контакти', class:'', link:'#'},
          {name:'Демо', class:'btn btn-large', link:'#'}
        ];
      };

  landingController.$inject = ['$scope'];

  angular.module('bizHubApp')
    .controller('landingController', landingController);
      
}());



