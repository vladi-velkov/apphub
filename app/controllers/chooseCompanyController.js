( function () {

  var chooseCompanyController = function ($scope, chooseCompanyFactory)
      {
        $scope.verticalNavigationItems = [
          {name:'Dashboard', class:'fa fa-home', link:'#'}
        ];

        $scope.myAppCompanies = [];
        $scope.myAppApps = [];

        function init()
          {
            $scope.myAppCompanies = chooseCompanyFactory.getCompanies();
            console.log($scope.myAppCompanies);
            $scope.myAppApps = chooseCompanyFactory.getApps();
          }
        init();
      };

  chooseCompanyController.$inject = ['$scope', 'chooseCompanyFactory'];

  angular.module('bizHubApp')
    .controller('chooseCompanyController', chooseCompanyController);
      
}());



