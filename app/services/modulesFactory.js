(function(){
    var modulesFactory = function() 
        {
            var companies =
                [
                  {
                    id: 1, 
                    name:'Компания 1', 
                    apps:
                      [
                        {
                          id: 1,
                          name: 'Склад', 
                          expire:'12/07/2015', 
                          start:'12/07/2014', 
                          users:'5/10', 
                          sales:'176/200', 
                          products:'81/100',
                          modules:
                            [
                              {
                                id: 1, 
                                name: 'Склад 1',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              },
                              {
                                id: 2, 
                                name: 'Склад 2',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              }
                            ]
                        },
                        {
                          id: 2, 
                          name: 'CRM', 
                          expire:'12/07/2015', 
                          start:'12/07/2014', 
                          users:'5/10', 
                          sales:'176/200', 
                          products:'81/100',
                          modules:
                            [
                              {
                                id: 1, 
                                name: 'CRM 1',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              },
                              {
                                id: 2, 
                                name: 'CRM 2',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              }
                            ]
                        },
                        {
                          id: 3, 
                          name: 'ERP',
                          expire:'12/07/2015', 
                          start:'12/07/2014', 
                          users:'5/10', 
                          sales:'176/200', 
                          products:'81/100', 
                          modules:
                            [
                              {
                                id: 1, 
                                name: 'ERP 1',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              },
                              {
                                id: 2, 
                                name: 'ERP 2',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              }
                            ]
                        }
                      ]
                  },
                  {
                    id: 2, 
                    name:'Компания 2', 
                    apps:
                      [
                        {
                          id: 1, 
                          name: 'Склад',
                          expire:'12/07/2015', 
                          start:'12/07/2014', 
                          users:'5/10', 
                          sales:'176/200', 
                          products:'81/100', 
                          modules:
                            [
                              {
                                id: 1, 
                                name: 'Склад 3',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              },
                              {
                                id: 2, 
                                name: 'Склад 4',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              }
                            ]
                        },
                        {
                          id: 2, 
                          name: 'CRM',
                          expire:'12/07/2015', 
                          start:'12/07/2014', 
                          users:'5/10', 
                          sales:'176/200', 
                          products:'81/100', 
                          modules:
                            [
                              {
                                id: 1, 
                                name: 'CRM 3',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              },
                              {
                                id: 2, 
                                name: 'CRM 4',
                                expire:'12/07/2015', 
                                start:'12/07/2014', 
                                users:'5/10', 
                                sales:'176/200', 
                                products:'81/100'
                              }
                            ]
                        }
                      ]
                  }
                ];

            var factory = {};

            factory.getCompanies = function () 
                {
                    return companies;
                };

            factory.getCompany = function(companyId)
                {
                    for(var k=0, lenK=companies.length; k<lenK; k++)
                    {
                        if(companies[k].id === parseInt(companyId))
                        {
                            console.log(companies[k])
                            return companies[k];
                        }
                    }
                }

            factory.getApps = function(companyId)
                {
                    for (var i=0, len=companies.length; i<len; i++)
                      {
                        if (companies[i].id === parseInt(companyId))
                        {
                            console.log(companies[i].apps);
                            return companies[i].apps;
                        }
                      }
                    return {};
                };

            factory.getApp = function(companyId, appId)
                {
                    for (var i=0, len=companies.length; i<len; i++)
                      {
                        if (companies[i].id === parseInt(companyId))
                        {
                            for (var j=0, lenJ=companies[i].apps.length; j<lenJ; j++)
                              {
                                if (companies[i].apps[j].id === parseInt(appId))
                                {
                                    console.log(companies[i].apps[j]);
                                    return companies[i].apps[j];
                                }
                              }
                        }
                      }
                    return {};
                };

            
            factory.getModules = function(companyId, appId)
                {
                    for (var i=0, len=companies.length; i<len; i++)
                      {
                        if (companies[i].id === parseInt(companyId))
                        {
                            for (var j=0, lenJ=companies[i].apps.length; j<lenJ; j++)
                              {
                                if (companies[i].apps[j].id === parseInt(appId))
                                {
                                    console.log(companies[i].apps[j].modules);
                                    return companies[i].apps[j].modules;
                                }
                              }
                        }
                      }
                    return {};
                };


            factory.getModule = function(companyId, appId, moduleId)
                {
                    for (var i=0, len=companies.length; i<len; i++)
                      {
                        if (companies[i].id === parseInt(companyId))
                            {
                                for (var j=0, lenJ=companies[i].apps.length; j<lenJ; j++)
                                  {
                                    if (companies[i].apps[j].id === parseInt(appId))
                                        {
                                            for (var k=0, lenK=companies[i].apps[j].modules.length; k<lenK; k++)
                                                {
                                                    if(companies[i].apps[j].modules[k].id === parseInt(moduleId))
                                                        {
                                                            console.log(companies[i].apps[j].modules[k]);
                                                            return companies[i].apps[j].modules[k];
                                                        }  
                                                }
                                        }
                                  }
                            }
                      }
                    return {};
                };

            return factory;
        };

    modulesFactory.$inject = ['$routeParams'];
    angular.module('bizHubApp').factory('modulesFactory', modulesFactory);
}());