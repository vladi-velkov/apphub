(function()
{
	var pricingFactory = function() {
			var packages = [
		          {
		          	id: 1,
		          	style:'panel-info', 
		          	row1:'Стартиращ търговец', 
		          	row2:'0', 
		          	row3:'1 Потребител', 
		          	row4:'1 Склад', 
		          	row5:'5 Клиента', 
		          	row6:'20 Артикула', 
		          	row7:'50 Продажби на месец', 
		          	row8:'-' 
		          },
		          {
		          	id: 2, 
		          	style:'panel-success', 
		          	row1:'Малък бизнес', 
		          	row2:'5', 
		          	row3:'2 Потребител', 
		          	row4:'2 Склад', 
		          	row5:'20 Клиента', 
		          	row6:'50 Артикула', 
		          	row7:'400 Продажби на месец', 
		          	row8:'-' 
		          },
		          {
		          	id: 3, 
		          	style:'panel-primary', 
		          	row1:'Среден бизнес', 
		          	row2:'9', 
		          	row3:'5 Потребител', 
		          	row4:'3 Склад', 
		          	row5:'50 Клиента', 
		          	row6:'200 Артикула', 
		          	row7:'1000 Продажби на месец', 
		          	row8:'Интеграция с портали за пазаруване' 
		          },
		          {
		          	id: 4, 
		          	style:'panel-danger', 
		          	row1:'Неограничен', 
		          	row2:'19', 
		          	row3:'Неограничени', 
		          	row4:'Неограничени', 
		          	row5:'Неограничени', 
		          	row6:'Неограничени', 
		          	row7:'Неограничени', 
		          	row8:'Интеграция с портали за пазаруване' 
		          }
		        ];
	        var userInformation = [
		          {
		            id: 1,
		            chosenPackage: 2,
		            email: 'velkov@gmail.com',
		            password: '123123',
		            paid: true
		          },
		          {
		            id: 2,
		            chosenPackage: 3,
		            email: 'mitov@gmail.com',
		            password: '123123',
		            paid: true
		          }
		        ];

        	var menuItems = [
		          {name:'Начало', class:'', link:'#/'},
		          {name:'Функционалности', class:'', link:'#'},
		          {name:'Цени', class:'active', link:'#/pricing'},
		          {name:'Контакти', class:'', link:'#'},
		          {name:'Демо', class:'btn btn-large', link:'#'}
		        ];

	        var factory = {};

			factory.setUser = function(user){
				userInformation[userInformation.length] = user;
				console.log(userInformation);
			}

			factory.getUsersLength = function() {
					return userInformation.length + 1;
				}

	        factory.getMenuItems = function() {
	        	return menuItems;
	        }

	        factory.getPackages = function () {
	        	return packages;
	        };

	        factory.getPackage = function(packageId){
	        	for (var i=0, len=packages.length; i<len; i++)
		          {

		            if (packages[i].id === parseInt(packageId))
		            {
		              return packages[i];
		            }
		          }
		          return {};
	        };
	        return factory;
		};
	angular.module('bizHubApp').factory('pricingFactory', pricingFactory);
}());