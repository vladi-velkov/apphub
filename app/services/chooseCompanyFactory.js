(function()
	{
		var chooseCompanyFactory = function () 
			{
				var companiesList = 
						[
								{
	      							id: 1, 
	      							name:'Right Click', 
	      							picture:'img/bg-close-person-hi.png', 
	      							phone:'0877 125 425', 
	      							mail:'info@rightclick.bg', 
	      							accessTo:
	      								[
	          								{
	          									id: 1, 
	          									name:'Склад', 
	          								},
	          								{
	          									id: 2, 
	          									name:'CRM', 
	          								}
	      								]
	      						},
								{
									id: 2, 
									name:'Long Life', 
									picture:'img/bg-close-person-hi.png', 
									phone:'0877 125 425', 
									mail:'info@rightclick.bg', 
									accessTo:
										[
											{
	          									id: 1, 
	          									name:'Склад', 
	          								}
										]
								},
								{
									id: 3, 
									name:'Alfa Vita', 
									picture:'img/bg-close-person-hi.png', 
									phone:'0877 125 425', 
									mail:'info@rightclick.bg', 
									accessTo:
										[
											{
	          									id: 1, 
	          									name:'Склад', 
	          								},
											{
	          									id: 2, 
	          									name:'CRM', 
	          								},
											{
	          									id: 3, 
	          									name:'ERP', 
	          								}
										]
								}
						];

				var factory = {};

				factory.getCompaniesLength = function() {
					return companiesList.length + 1;
				}

				factory.getCompanies = function ()
					{
						console.log(companiesList);
						return companiesList;
					}

				factory.setCompanies = function(company){
					companiesList[companiesList.length] = company;
					console.log(companiesList);
				}

				factory.getApps = function()
	                {
	                    for (var i=0, len=companiesList.length; i<len; i++)
	                      {
                            console.log(companiesList[i].accessTo);
                            return companiesList[i].accessTo;
	                      }
	                    return {};
	                };

				return factory;

			};

    	angular.module('bizHubApp').factory('chooseCompanyFactory', chooseCompanyFactory);
	}());