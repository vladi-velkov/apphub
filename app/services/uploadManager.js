(function()
{
    var uploadManager =  function ($rootScope) {
    var _files = [];
    return {
        add: function (file) { console.log('add');
            _files.push(file);
            $rootScope.$broadcast('fileAdded', file.files[0].name);
        },
        clear: function () {console.log('clear');
            _files = [];
        },
        files: function () {console.log('files');
            var fileNames = [];
            $.each(_files, function (index, file) {
                fileNames.push(file.files[0].name);
            });
            return fileNames;
        },
        upload: function () {console.log('upload');
            $.each(_files, function (index, file) {
                file.submit();
            });
            this.clear();
        },
        setProgress: function (percentage) {console.log('setProgress');
            $rootScope.$broadcast('uploadProgress', percentage);
        }
    };
};


  angular.module('bizHubApp').directive('upload', ['uploadManager', function factory(uploadManager) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).fileupload({
                dataType: 'text',
                add: function (e, data) {
                    uploadManager.add(data);
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    uploadManager.setProgress(progress);
                },
                done: function (e, data) {
                    uploadManager.setProgress(0);
                }
            });
        }
    };
}]);
  


angular.module('bizHubApp').factory('uploadManager', uploadManager);
}());





