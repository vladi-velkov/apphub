( function () {
	var bizHubApp = angular.module('bizHubApp', ['ngRoute', 'ngAnimate']);

	angular.module('bizHubApp').config (function ($routeProvider) {
    $routeProvider
        .when('/',
            {
              controller: 'landingController',
              templateUrl: 'app/views/landing.html'
            })
        .when('/pricing',
            {
              controller: 'pricingController',
              templateUrl: 'app/views/pricing-step1.html'
            })
        .when('/plans/:packageId',
            {
              controller: 'pricingController',
              templateUrl: 'app/views/pricing-step2.html'
            })
        .when('/payment/:packageId',
            {
              controller: 'pricingController',
              templateUrl: 'app/views/pricing-step3.html'
            })
        .when('/login',
            {
              controller: 'loginController',
              templateUrl: 'app/views/login.html'
            })
        .when('/recover',
            {
              controller: 'recoveryController',
              templateUrl: 'app/views/recover.html'
            })
        .when('/dashboard/:companyId/:appId/:moduleId',
            {
              controller: 'dashboardController',
              templateUrl: 'app/views/dashboard.html'
            })
        .when('/dashboard/:companyId/:appId',
            {
              controller: 'dashboardController',
              templateUrl: 'app/views/chooseModule.html'
            })
         .when('/dashboard/:companyId',
            {
              controller: 'dashboardController',
              templateUrl: 'app/views/chooseApp.html'
            })
        .when('/dashboard',
            {
              controller: 'chooseCompanyController',
              templateUrl: 'app/views/chooseCompany.html'
            })
        .when('/create-company',
            {
              controller: 'createCompanyController',
              templateUrl: 'app/views/createCompany.html'
            })
//        .otherwise({ redirectTo: '/'});

  });

}());